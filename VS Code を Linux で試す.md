---

title: VS Code を Linux で試す
description: 
url:
page_number: true
footer: @matoken
marp: true

---
## VS Code を Linux で試す

Kenichiro MATOHARA (@matoken)
https://matoken.org

鹿児島Linux勉強会 2019.05(2019-05-11)

---
## 最近

* 先月の発表のvmdb2 でRaspberry Pi のDebian buster amd64 イメージ作成その後
  ファイルシステムをF2FS にしてストレージに優しく?
  初期ユーザ初回ログイン時にパスワード変更を強制
  RTC対応(未リリース)
  GitHub release でイメージ配布開始
  とかとか
* 現在はvmdb2 でいじれない細かいところをbash script にしてみている
  F2FS crypt optionを有効に
  armhf 対応
  Raspbian 対応
  もう少しいじったらAnsible Playbook化したい

---
最近Marp (Next) のVS Code extention を試してElectron なのに案外軽いなということでVS Code を触り始めた

* [Visual Studio Code - Code Editing. Redefined](https://code.visualstudio.com/)
* [GitHub - marp-team/marp-vscode: Marp for VS Code: Preview Marp slide deck in VS Code](https://github.com/marp-team/marp-vscode)

---
## 入手

マルチプラットホームでLinux もdeb/rpm/tar.gz や snap package がある
Archtecture はそれぞれ amd64, i686

---
## snap版導入

```
$ sudo apt install snapd
$ echo "$PATH:/snap/bin" >> ~/.bashrc
$ source ~/.bashrc
$ sudo snap install code --classic
$ code
```

---
## Microsoftによるテレメトリ機能を無効化

VS Code にはMicrosoft によるテレメトリ機能が付いているようでちょっと気持ち悪い．
設定変更で回避出来るようだが，実際に回避できるかは未確認．

![](https://i.postimg.cc/tRMpDQrh/20190427-03-04-10-25261.jpg)

> If you don't wish to send usage data to Microsoft, you can set the telemetry.enableTelemetry setting to false.
* [Visual Studio Code Frequently Asked Questions](https://code.visualstudio.com/docs/supporting/faq#_how-to-disable-telemetry-reporting)

---
## VSCodium

* [VSCodium - The advanced editor](https://vscodium.com/)

Microsoftによるテレメトリが無効にされたfork
少し試しただけではVS Code との差がわからない(アイコンなどは違う)

Debian/Ubuntu環境への導入例
```
$ wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -
$ echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
$ sudo apt update && sudo apt install vscodium
$ vscodium
```

---
![](https://i.postimg.cc/8zPc6LwZ/20190427-04-04-08-12437.jpg[70%])

---
## ARM対応

今のところVS Code/VSCodium でARMバイナリは提供されていない(ARM64はもうすぐ来そう)
自分でビルドするのは面倒……

* [Visual Studio Code for Chromebooks and Raspberry Pi](https://code.headmelted.com/)
  amd64, i386, armhf, arm64 に対応したVS Code OSS版のコミニュティビルド
  Chrome OS や Raspberry Pi でも!

---

最近 ARM64 環境で使っているけど(RAMがあるうちは)さくさく動く
Vim, ReTextと併用中
