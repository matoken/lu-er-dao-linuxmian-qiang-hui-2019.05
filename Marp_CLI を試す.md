---
title: Marp Next を試す
description: 
url:
page_number: true
footer: @matoken
marp: true

---

## Marp Next を試す

Kenichiro MATOHARA (@matoken)
https://matoken.org

鹿児島Linux勉強会 2019.05(2019-05-11)

---
## 最近

* 先月の発表のvmdb2 でRaspberry Pi のDebian buster amd64 イメージ作成その後
  ファイルシステムをF2FS にしてストレージに優しく?
  初期ユーザ初回ログイン時にパスワード変更を強制
  RTC対応(未リリース)
  GitHub release でイメージ配布開始
  とかとか
* 現在はvmdb2 でいじれない細かいところをbash script にしてみている
  F2FS crypt optionを有効に
  armhf 対応
  Raspbian 対応
  もう少しいじったらAnsible Playbook化したい

---
## Marp Next を試す

---
## スライド作成ソフト何を使っていますか?

* Microsoft PowerPoint
* Apple Keynode
* Google Slide
* LibreOffice Impress
 :

---
## matokenの場合

* 好きなテキストエディタで書いてcli でスライド形式に変換できると環境を選ばなくなって嬉しい
* 出先でいじったりするので軽いものが嬉しい
* 後でblog にしたりするのでマークアップ言語などが都合が良い(VCSでの管理もできるし(Flat ODFでもまあ))
  * OpenOffice.org -> Keynote -> LibreOffice -> Prezi -> Google Slide -> Tex -> Reveal.js -> Marp
  少し前はMarp(Markdown) をよく使っていて最近は asciidoc + Reveal.js(include が便利，オフレコ部分を別ファイルにして.gitignoreに登録したり)

---
## Marp

* Markdown を書くだけで綺麗なスライドに
* pdf 書き出しもできるので公開も便利

  現在は開発が終了して Marp Next が開発中
![](https://github.com/marp-team/marp/raw/master/marp.png)

---
## Marp Next!

* 開発が進んで使えるようになっている
* Core といくつかのアプリケーションがある
* Desktop版はまだない
* 一般向けでは VS Code Extention や Web版が便利そう
* CLI版の Marp-CLI が気になる!

[GitHub \- marp\-team/marp\-cli: A CLI interface for Marp and Marpit based converters](https://github.com/marp-team/marp-cli)

---

## hello

* hoge
* fuga

---

## にゃーん

* `:cat:` -> :cat:
* :+1:

---

## こんな感じ

![](image/source.jpg)

---

## 試し方

* npx / npm / Docker
* とりあえず npx で動かすのがお手軽(要onlineなので出先等で触る場合は注意)

---
## Ubuntu 19.04 arm64 npm での導入例

```
$ sudo apt install nodejs npm
$ echo 'PATH=$PATH:${HOME}/node_modules/.bin' >> ~/.profile
$ cd
$ source ~/.profile
$ npm install --save-dev @marp-team/marp-cli
$ marp -v
@marp-team/marp-cli v0.8.1 (/w bundled @marp-team/marp-core v0.8.0)
```

* nodejs/npm のバージョンが合わない場合はこのへんから入手するなどする
 [ダウンロード \| Node\.js](https://nodejs.org/ja/download/)
 [パッケージマネージャを利用した Node\.js のインストール \| Node\.js](https://nodejs.org/ja/download/package-manager/)

---
## 主なオプション

* marp ./slide-deck.md	# htmlに変換
* marp ./slide-deck.md --pdf	# pdfに変換(Google Chrome利用)
* marp ./slide-deck.md --image # 画像(png/jpg)に変換
* marp ./slide-deck.md -o converted.[html/pdf/png/jpg]
* marp -w ./slide-deck.md 	# ウォッチモード
* marp -s ./slidedir		# サーバモード(ディレクトリ指定)
* marp -p ./slide-deck.md	# プレビューウィンドウ

---

## ウォッチモード/サーバモード/プレビュー

* ウェブブラウザで変換結果が確認できる
* ファイル更新時に自動再レンダリングされる(ブラウザの自動更新機能は不要)
* 好きなテキストエディタで編集
* サーバモードではhttp経由で確認
  リモートアクセスも出来るのでtcp8080を許可して同じネットワークの人たちで共有できる
  pdf書き出しもリンクを押すだけで出来る

---

## ローカルのファイルを使いたい

* 既定値では `-o slide.pdf` や サーバモードでの pdf 書き出しで画像が書き出されない
* ローカルファイルを使う場合はオプションが必要
  以下 **README.md** より
```
  --allow-local-files Allow to access local files from Markdown while converting PDF (NOT SECURE)
```

---

## html を使いたい

* 既定値ではhtml は解釈されずそのまま表示されるので `--html` オプションを使う
```
--html  Enable or disable HTML (Configuration file can pass the whitelist object if you are using Marp Core)
```

<!--
※URLが削られてしまうことがある?

```
## source

https://gist.github.com/5da13dacaca0443c619108943db1a3bf
```
が以下のようにURL途中で途切れた

![](./image/url.jpg)

\`\`で囲んでごまかしてみたり
-->

---
## GUI?

* Marp Web，Marp VSCode(VSCodeのextention)が存在する
* その他Marp Webのデスクトップ版がplanned
  * [marp\-team/marp\-web: \[UNDER CONSTRUCTION\] Markdown presentation writer for web interface](https://github.com/marp-team/marp-web)
  * [marp\-team/marp\-vscode: Marp for VS Code: Preview Marp slide deck in VS Code](https://github.com/marp-team/marp-vscode)

---

## Marp Web

https://web.marp.app/ で試せる
![](image/marp_web.jpg)

---
## Marp VS Code extention

* おすすめ
* Microsoft製OSSエディタのextention
* VS Code はElectronてことで重そうで敬遠していたけど案外軽い
* VS CodeのARM版も公式以外のビルドだが存在するのでSBCなどでも動作する(時間があれば別スライドで紹介)
* ライブプレビューが便利
  ……しかし編集部分とプレビュー部分がずれる

---
## Marp VS Code extention
<!--
![](image/marp_vscode01.resized.jpg)

---
-->

![](image/marp_vscode02.resized.jpg)

---
## Marp VS Code extention

`marp: true` を設定するとプレビューがスライド形式になる  
![](image/marp_vscode03.resized.jpg)

---
## 旧Marpと比べて

* Electron -> Node(Desktop版はElectronで包むかも?)
* cli で動く :)
* ARM端末(armhf/arm64で確認)でも動く :)
  (旧Marpでも行けたのかもだけど手元ではbuild失敗してた)

---
## 試した環境1(Marp CLI, Marp VS Code extention)

```
$ marp -v
@marp-team/marp-cli v0.8.1 (/w bundled @marp-team/marp-core v0.8.0)
$ dpkg-query -W google-chrome-stable chromium vscodium
chromium        73.0.3683.75-1
google-chrome-stable    73.0.3683.103-1
vscodium        1.33.1-1555005058
$ code -v
1.33.1
51b0b28134d51361cf996d2f0a1c698247aeabd8
x64
$ jq .version ~/.vscode/extensions/marp-team.marp-vscode-0.2.0/package.json 
"0.2.0"
$ lsb_release -dr
Description:    Debian GNU/Linux buster/sid
Release:        unstable
$ uname -m
x86_64
```

---
## 試した環境2(Marp CLI, Marp VS Code extention)

```
$ marp -v
@marp-team/marp-cli v0.8.1 (/w bundled @marp-team/marp-core v0.8.0)
$ dpkg-query -W chromium-browser
chromium-browser        73.0.3683.86-0ubuntu0.18.04.1
$ lsb_release -d
Description:    Ubuntu 18.04.2 LTS
$ code-oss -v
1.32.0
aeaef41d51201e555735f5e8d2f38a9d0ddb9026
arm64
$ jq .version ~/.vscode-oss/extensions/marp-team.marp-vscode-0.2.0/package.json
"0.2.0"
$ uname -m
aarch64
```

---

## 試した環境3(Marp CLI)
```
$ marp -v
@marp-team/marp-cli v0.8.1 (/w bundled @marp-team/marp-core v0.8.0)
$ nodevs -v
v10.15.3
$ npm -v
6.9.0
$ lsb_release -d
Description:    Debian GNU/Linux 8.11 (jessie)
$ uname -m
armv7l
$ dpkg --print-architecture
armhf
```

---

## source

`https://gist.github.com/5da13dacaca0443c619108943db1a3bf`

