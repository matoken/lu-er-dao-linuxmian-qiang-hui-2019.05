#!/bin/sh

find . -type f -iname "*.md" ! -name "README.md" -print0 | xargs -0 -n1 marp --allow-local-files --html --pdf

